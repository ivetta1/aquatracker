import { Router } from 'express';
import signup from '../handlers/users/signup';
const { validateBody, authenticate, upload } = require('../../middlewares');
const { schemas } = require('../../models/user');

const router = Router();

router.post('/signup', signup);
// router.post('/signin', validateBody(schemas.signinSchema), ctrl.signin);
// router.get('/current', authenticate, ctrl.currentUser);
// router.patch('/current/edit', authenticate, upload.single('avatar'), validateBody(schemas.updateUserSchema), ctrl.updateUser);
// router.get('/current/refresh', ctrl.refreshCurrentUser);
// router.post('/signout', authenticate, ctrl.signout);

export default router;