export interface User {
    name: string,
    email: string,
    avatarURL: string | null,
    gender: "male" | "female" | null,
    weight: number | null,
    activeTime: number | null,
    waterDailyNorma: number,
    token: string,
    refreshToken: string
}