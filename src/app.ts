import express, { Application, Request, Response } from 'express';
import usersRouter from './routes/users';

const app: Application = express();
const port = 3000;

app.use('/api/users', usersRouter)


app.use((req: Request, res: Response) => {
    res.status(404).json({ message: 'Service not found' });
});

// app.use((err, req: Request, res: Response, next: NextFunction) => {
//     const { status = 500, message = 'Server error' } = err;
//     res.status(status).json({ message });
// });


app.listen(port, () => {
    console.log(`Connected successfully on port ${port}`)
})